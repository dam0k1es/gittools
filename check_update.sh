#!/bin/bash

#Place this script in a folder that has repositories as sub-folders.

#Iterate over all subdirectorys
#and start a subshell
for d in */ ;
do
    (
    #Go into a subdirectory
    cd "$(readlink -f "$d")" || exit

    #Save Path of Subdirectory
    wd="$(basename "$(pwd)")"

    #Check if a remote repository is set
     git remote -v | grep -q 'https\|ssh' || exit

    #Get the HEAD of the remote Repository,
    #look for the local string HEAD
    #and print the status based on the result
    git ls-remote origin | \
    grep "$(git rev-parse HEAD)" -q && \
    echo -e "\033[0;32m""$wd""\033[0m" is up to date || \
    echo -e "\033[0;31m""$wd""\033[0m" needs to be pulled

    #Check if a local change has been made
    status="$(git status -s)"
    [[ "$status" != "" ]] && echo -e "\n\033[0;31m""$(pwd)"":\033[0m\n""$status"
    )
done
