#!/bin/bash
#set -x

#Put this script in a repository called dotfiles. 
#From there, the dot files in the repository
#can be compared with those on the system.


#If a file differs from the original
#display methods to view the diffs
diffmenu(){
    #Show the Menu
    answer=$(dialog --backtitle "diffmenu" \
    --menu "How do you want to show the diff for '$f' ?" 0 0 0 \
    "Normal" "" "Side-by-Side" ""  "Interactive" "" "Skip" "" "Exit" "" 3>&1 1>&2 2>&3)

    #Clear the Screen
    dialog --clear ; clear

    #Operate
    if [ "$answer"  = "Normal" ] ; then
        diff "$f" "$orig" --color=always
        echo Press enter to coninue
        read -r any
    fi
    
    [ "$answer"  = "Side-by-Side" ] \
    && diff -y "$f" "$orig" --color=always | less
    
    [ "$answer"  = "Interactive" ] \
    && vimdiff "$f" "$orig" 

    [ "$answer"  = "Skip" ] \
    && return 1 
    
    [ "$answer"  = "Exit" ] \
    && exit 
    
    #Return 0 to continue the loop
    return 0
}

#If a file differs from the original
#display the option to overwrite the target with the original
#and the other way round
cpmenu(){
    answer=$(dialog --backtitle "cpmenu" \
    --menu "Do you want to overwrite '$f' ?" 0 0 0 \
    "Yes" "" "Reverse" "" "No" "" 3>&1 1>&2 2>&3)

    [ "$answer" = "Yes" ] && yes | /bin/cp -frL "$orig" "$f" 
    [ "$answer" = "Reverse" ] && yes | /bin/cp -frL "$f" "$orig"
}

#List all files except git config files and images,
#as well as diff.sh and README.md
fetch_files(){
    [ -d .git ] || exit
    files=$(find . \
    -type d \( -name ".git" -or -name "img" \) -prune \
    -o -type f \( -name "diff.sh" -or -name "README.md" -or -name ".gitignore" -or -name "TODO.md" -or -name "wpg.rasi" \) -prune \
    -o -type f \
    -exec readlink -f  {} \; )
}

#Send Notification
send(){
    [[ $notify == "echo" ]] && echo -e "$1""\033[1;33m""$2""\033[0m""$3"
    [[ $notify == "notify-send" ]] && notify-send "$1" "$2"
}

#Set Output Format
format=$1
[[ -z $format ]] && format="--short"
[[ $format =~ ^(-s|-l|--short|--long)$ ]] || exit 1

#Set Notification Mode
notify=$2
[[ -z $notify ]] && notify="echo"
[[ $notify =~ ^(echo|notify-send)$ ]] || exit 1

#Fetch files
fetch_files

#Compare all files in the list with the original dotfiles
#and send a notification if they differ, or are not found
for f in $files;
do
    orig="$HOME/$( echo "$f" | sed 's/.*dotfiles\///')"
    if [ -f "$orig" ] ;
    then
        if [[ "$(diff -q "$f" "$orig" )" ]] ;
        then
            [[ "$format" == @(--long|-l) ]] && clear
            send "$f" " has changed"
            [[ "$format" == @(--long|-l) ]] \
            && diffmenu "$orig" \
            && cpmenu "$orig" "$f"
        fi
    else
        send "$orig" " not found"
    fi
done

[[ "$format" == @(--long|-l) ]] && dialog --clear
