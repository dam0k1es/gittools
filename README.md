# gittools

This repository provides tools that should simplify the handling of repositories in a CLI-based environment.

- **check_update.sh**:
checks repositories in a folder for updates without executing them

- **check_dotfiles.sh**:
checks dotfiles in a local repository for local updates and offers to copy them
